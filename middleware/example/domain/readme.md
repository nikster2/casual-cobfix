
# casual domain examples

## pre requirements

Make sure you have casual installed and `CASUAL_HOME` set to the installed directory

Make sure you have a resource configuration. If not, copy the example resorce file:
    
    host$ cp $CASUAL_HOME/configuration/example/resources.yaml $CASUAL_HOME/configuration/
    
    

## examples

### single domain

Examples that uses only one domain

#### minimal domain
An example of a bare bone domain

[minimal domain]( single/minimal/readme.md)


### multiple domains

#### minimal
An example of a minimal setup for two domains

[minimal domain]( multiple/minimal/readme.md)


#### medium
An example of a _medium_ setup for two domains

[medium domain]( multiple/medium/readme.md)
