domain:
  name: domain.A42
  
  #
  # Default values for some of the possible settings
  default:
    environment:
      #
      # includes other environment files, that has to have the
      # same content as this block. That is:
      # environment:
      #    files: ...
      #    variables ...
      #
      # You can of course use any of yaml, xml, json or ini format, as
      # you could for this domain configuration file
      #
      files:
        - /some/path/to/environment/file.yaml
        - /some/other/file.json

      
      variables:
        # analogt to 'export SOME_VARIABLE=42'
        - key: SOME_VARIABLE
          value: 42

        - key: SOME_OTHER_VARIABLE
          value: some value

    # Default values for servers
    server:
      instances: 1
      restart: true

    # defualt values for executables (ie not casual servers)
    executable:
      instances: 1

    # default values for services
    service:
      timeout: 90s


  # transaction related configuration
  transaction:
    # default values that is used if not specified/override explicitly by a resource
    default:
      resource:
        
        # this key has to be 'defined' in ${CASUAL_HOME}/configuration/resources.(yaml|xml|...).
        # that file is used by the transaction manager to know which resource proxy to use.
        key: TMSUDB2
        instances: 3


    # Where the persistent transaction log will be stored.
    # If not specified: defaults to ${CASUAL_DOMAIN_HOME}/transaction/log.db
    log: /some/fast/disk/domain.A42/transaction.log
    
    #
    # Here are the resources defined. This configuration is then used by servers that
    # is built with one (or more) resources (keys) and either has a direct dependency in this
    # configuration file to the resource name (customer-db for example) or indirect via
    # a group.
    # This configuration is also used by the transaction-manager to get the correct open-info
    # and number of instances such for each resource-proxy.
    #
    resources:
      - instances: 5 # overrides from default
        name: customer-db
        openinfo: db=customer,uid=db2,pwd=db2 # this is not the actual syntax for db2 open-info...

      - name: sales-db  # uses the default instances
        openinfo: db=sales,uid=db2,pwd=db2

      - key: mq_rm  # overrides and uses another rm-key
        name: event-queue
        openinfo: some-mq-specific-stuff
        closeinfo: some-mq-specific-stuff

  groups:
    # you can specify any number of groups and relations between them
    - name: common-group

    - name: customer-group
      note: group that logically groups 'customer' stuff
      resources:
        - customer-db  # this group 'uses' the 'customer-db' resource
      dependencies:
        - common-group  # and has dependency to the 'common-group'

    - name: sales-group
      note: group that logically groups 'customer' stuff
      resources:
        - sales-db
        - event-queue  #this group has two resources 

      dependencies:
        - customer-group # dependency to 'customer-group'



  #
  # Here we define the servers that will run in this domain
  servers:
    
    # PATH has to include path to find customer-server-1
    - path: customer-server-1
      memberships:
        - customer-group # implicit dependency to resource 'customer-db'


    - path: customer-server-2
      memberships:
        - customer-group


    - path: sales-server
      alias: sales-pre  # use an alias to distinguish this server from other (whith the same binary)
      instances: 10
      memberships:
        - sales-group

      # the only services that will be advertised are 'preSalesSaveService' and 'preSalesGetService'
      restrictions:
        - preSalesSaveService
        - preSalesGetService


    - path: sales-server
      alias: sales-post # use an alias to distinguish this server from other (whith the same binary)
      memberships:
        - sales-group

      # the only services that will be advertised are 'postSalesSaveService' and 'postSalesGetService'
      restrictions:
        - postSalesSaveService
        - postSalesGetService

    - path: sales-broker
      memberships:
        - sales-group

      environment:
        # Specific environment variable for this server
        variables:
          - key: SALES_BROKER_VARIABLE
            value: 556
            
      # explicitly get configuration for a resource, note that this server also get
      # implicit resource configuration from the group 'sales-group'
      resources:
        - event-queue  


  #
  # Here we define which executables that this domain should start. Executables
  # are executable program that does not know about casual, hence does not 
  # expose any xatmi-services and such.
  #
  executables:
    - path: mq-server
      arguments:
        - --configuration
        - /path/to/configuration

      memberships:
        - common-group



  #
  # Definition of attributes for specific services
  services:
    # the name of the service
    - name: postSalesSaveService
      timeout: 2h # timeout. The unit can be: h (hours), min (minutes), s (seconds), ms (milliseconds) us (microseconds)
      
      # routes defines other names for the service.
      # If you want to keep the original name you need to include in the route.
      routes:
        - postSalesSaveService
        - sales/post/save


    - timeout: 130ms
      name: postSalesGetService


  #
  # 
  gateway:
    default:
      connection:
        type: tcp
        restart: true


    listeners:
      - address: localhost:7779
        note: local host

      - address: some.host.org:7779
        note: another listener that is bound to some 'external ip'


    connections:
      - address: a45.domain.host.org:7779
        note: connection to domain 'a45' - we expect to find service 's1' and 's2' there.
        services:
          - s1
          - s2

        queues:
          []


      - address: a46.domain.host.org:7779
        note: connection to domain 'a46' - we expect to find queues 'q1' and 'q2' and service 's1' - casual will only route 's1' to a46 if it's not accessible in a45 (or local)
        services:
          - s1

        queues:
          - q1
          - q2


      - restart: false
        address: a99.domain.host.org:7780
        note: connection to domain 'a99' - if the connection is closed from a99, casual will not try to reestablish the connection
        services:
          []

        queues:
          []




  queue:
    default:
      queue:
        retries: 0

      directory: ${CASUAL_DOMAIN_HOME}/queue/groups

    groups:
      - name: groupA
        note: "will get default queuebase: ${CASUAL_DOMAIN_HOME}/queue/groupA.gb"
        queues:
          - name: q_A1
            note: ""

          - retries: 10
            name: q_A2
            note: after 10 rollbacked dequeues, message is moved to q_A2.error

          - name: q_A3
            note: ""

          - name: q_A4
            note: ""



      - name: groupB
        queuebase: /some/fast/disk/queue/groupB.qb
        note: ""
        queues:
          - name: q_B1
            note: ""

          - retries: 10
            name: q_B2
            note: after 10 rollbacked dequeues, message is moved to q_B2.error



      - name: groupC
        queuebase: ":memory:"
        note: group is an in-memory queue, hence no persistence
        queues:
          - name: q_C1
            note: ""

          - name: q_C2
            note: ""





