# transaction `casual CLI`

```
  transaction  0..1
        transaction related administration

    SUB OPTIONS
      -lt, --list-transactions  0..1
            list current transactions

      -lr, --list-resources  0..1
            list all resources

      -li, --list-instances  0..1
            list resource instances

      -ui, --update-instances  0..1  (<value>...) [0..* {2}]
            update instances - -ui [<rm-id> <# instances>]+

      -lp, --list-pending  0..1
            list pending tasks

      --state  0..1  ([json,yaml,xml,ini]) [0..1]
            view current state in optional format

```