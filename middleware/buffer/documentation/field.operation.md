# field operation

## environment

If the application is using a casual-field-repository, the environment-variable CASUAL\_FIELD\_TABLE needs to be present and its value should be the repository-file e.g.

`$ export CASUAL_FIELD_TABLE=$HOME/casual-field-repositories/field.json`

The repository-file could be either [JSON](./../sample/field.json), [XML](./../sample/field.xml), [INI](./../sample/field.ini) or [YAML](./../sample/field.yaml)
