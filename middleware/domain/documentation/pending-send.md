# casual-domain-pending-message

Responsible for eventually send messages to their destinations.

Used by other (mostly managers) functionality to simplify when a 
message is not able to be sent at a given time. 
