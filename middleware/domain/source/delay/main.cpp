//! 
//! Copyright (c) 2015, The casual project
//!
//! This software is licensed under the MIT license, https://opensource.org/licenses/MIT
//!



#include "domain/delay/delay.h"


int main( int argc, char **argv)
{
   return casual::domain::delay::main( argc, argv);
}
