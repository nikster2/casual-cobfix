//! 
//! Copyright (c) 2015, The casual project
//!
//! This software is licensed under the MIT license, https://opensource.org/licenses/MIT
//!


#include "service/manager/handle.h"
#include "service/manager/admin/server.h"
#include "service/transform.h"
#include "service/common.h"

#include "common/server/lifetime.h"
#include "common/environment.h"
#include "common/algorithm.h"
#include "common/process.h"
#include "common/message/dispatch.h"
#include "common/message/handle.h"
#include "common/event/listen.h"

#include "common/communication/instance.h"

#include "domain/pending/message/send.h"

// std
#include <vector>
#include <string>

namespace casual
{
   using namespace common;

   namespace service
   {
      namespace manager
      {
         namespace ipc
         {
            const common::communication::ipc::Helper& device()
            {
               static communication::ipc::Helper singleton{ communication::error::handler::callback::on::Terminate{ &handle::process_exit}};
               return singleton;
            }
         } // ipc

         namespace handle
         {
            namespace local
            {
               namespace
               {
                  namespace optional
                  {
                     template< typename D, typename M>
                     bool send( D&& device, M&& message)
                     {
                        try
                        {
                           log::line( verbose::log, "send message: ", message);
                           ipc::device().blocking_send( device, message);
                           return true;
                        }
                        catch( const common::exception::system::communication::Unavailable&)
                        {
                           log::line( log, "ipc unavailable: ", device);
                           return false;
                        }
                     }
                  } // optional

                  namespace eventually
                  {
                     namespace detail
                     {
                        namespace get
                        {
                           template< typename D>
                           using is_process =  std::is_same< traits::remove_cvref_t< D>, common::process::Handle>;

                           template< typename D>
                           std::enable_if_t< is_process< D>::value, const common::process::Handle&>
                           process( D&& device) { return device;} 

                           template< typename D> 
                           std::enable_if_t< ! is_process< D>::value, const common::process::Handle&>
                           process( D&& device) { return device.connector().process();}

                           template< typename P, typename = std::enable_if_t< is_process< P>::value, strong::ipc::id>>
                           decltype( auto) device( P&& process) { return process.ipc;}
                           
                           template< typename D, typename = std::enable_if_t< ! is_process< D>::value>>
                           decltype( auto) process( D&& device) { return std::forward< D>( device);}

                        } // get
                     } // detail
                     template< typename D, typename M>
                     void send( D&& device, M&& message)
                     {
                        Trace trace{ "service::manager::handle::local::eventually::send"};

                        log::line( verbose::log, "message: ", message);

                        try
                        {
                           if( ! communication::ipc::non::blocking::send( detail::get::device( device), message, ipc::device().error_handler()))
                              casual::domain::pending::message::send( detail::get::process( device), std::forward< M>( message), ipc::device().error_handler());
                        }
                        catch( const common::exception::system::communication::Unavailable&)
                        {
                           log::line( log, "destination unavailable - ", device);
                        }

                     }
                  } // eventually

                  namespace metric
                  {
                     void send( State& state)
                     {
                        auto pending = state.events( state.metric.message());
                        state.metric.clear();

                        if( ! common::message::pending::non::blocking::send( pending, manager::ipc::device().error_handler()))
                           casual::domain::pending::message::send( pending);
                     }
                  } // metric
               } // <unnamed>
            } // local

            namespace metric
            {
               void send( State& state)
               {
                  if( state.metric)
                     local::metric::send( state);
               }

               namespace batch
               {
                  void send( State& state)
                  {
                     if( state.metric.size() >= platform::batch::service::metrics)
                        local::metric::send( state);
                  }
               } // batch
            } // metric


            void process_exit( const common::process::lifetime::Exit& exit)
            {
               // We put a dead process event on our own ipc device, that
               // will be handled later on.
               common::message::event::process::Exit event{ exit};
               communication::ipc::inbound::device().push( std::move( event));
            }



            namespace process
            {
               namespace local
               {
                  namespace
                  {
                     template< typename I>
                     void service_call_error_reply( State& state, const I& instance)
                     {
                        Trace trace{ "service::manager::handle::process::local::service_call_error_reply"};

                        log::line( common::log::category::error, "callee terminated with pending reply to caller - callee: ", 
                           instance.process.pid, " - caller: ", instance.caller().pid);

                        log::line( common::log::category::verbose::error, "instance: ", instance);

                        common::message::service::call::Reply message;
                        message.correlation = instance.correlation();
                        message.code.result = common::code::xatmi::service_error; 

                        handle::local::eventually::send( instance.caller(), std::move( message));
                     }

                  } // <unnamed>
               } // local

               void Exit::operator () ( common::message::event::process::Exit& message)
               {
                  Trace trace{ "service::manager::handle::process::Exit"};

                  log::line( verbose::log, "message: ", message);

                  // we need to check if the dead process has anyone wating for a reply
                  if( auto found = common::algorithm::find( m_state.instances.sequential, message.state.pid))
                     if( found->second.state() == state::instance::Sequential::State::busy)
                        local::service_call_error_reply( m_state, found->second);

                  m_state.remove( message.state.pid);
               }

               namespace prepare
               {
                  void Shutdown::operator () ( common::message::domain::process::prepare::shutdown::Request& message)
                  {
                     Trace trace{ "service::manager::handle::process::prepare::Shutdown"};

                     log::line( verbose::log, "message: ", message);

                     auto reply = common::message::reverse::type( message, common::process::handle());
                     reply.processes = std::move( message.processes);

                     auto deactivate_process = [&]( auto& process)
                     {
                        m_state.deactivate( process.pid);
                     };

                     // all we need to do is to deactivate all servers (remove all 
                     // associated services). domain-manager will then send shutdown to
                     // the servers, and we'll receive a process::Exit when the server 
                     // has exit, and we'll clean up the rest.
                     algorithm::for_each( reply.processes, deactivate_process);

                     handle::local::eventually::send( message.process, reply);
                  }

               } // prepare
            } // process

            namespace event
            {
               namespace subscription
               {
                  void Begin::operator () ( common::message::event::subscription::Begin& message)
                  {
                     Trace trace{ "service::manager::handle::event::subscription::Begin"};
                     log::line( verbose::log, "message: ", message);

                     m_state.events.subscription( message);
                  }

                  void End::operator () ( common::message::event::subscription::End& message)
                  {
                     Trace trace{ "service::manager::handle::event::subscription::End"};
                     log::line( verbose::log, "message: ", message);

                     m_state.events.subscription( message);
                  }
               } // subscription
            } // event

            namespace service
            {
               void Advertise::operator () ( common::message::service::Advertise& message)
               {
                  Trace trace{ "service::manager::handle::service::Advertise"};
                  log::line( verbose::log, "message: ", message);

                  m_state.update( message);
               }

               namespace concurrent
               {
                  void Advertise::operator () ( common::message::service::concurrent::Advertise& message)
                  {
                     Trace trace{ "service::manager::handle::service::concurrent::Advertise"};
                     common::log::line( verbose::log, "message: ", message);

                     m_state.update( message);
                  }

                  void Metric::operator () ( common::message::event::service::Calls& message)
                  {
                     Trace trace{ "service::manager::handle::service::concurrent::Metric"};
                     log::line( verbose::log, "message: ", message);

                     auto update_metric = [&]( auto& metric)
                     {
                        if( auto service = m_state.find_service( metric.service))
                           service->metric.update( metric);
                     };

                     algorithm::for_each( message.metrics, update_metric);

                     if( m_state.events)
                     {
                        m_state.metric.add( std::move( message.metrics));
                        handle::metric::batch::send( m_state);
                     }
                  }
               } // concurrent


               void Lookup::operator () ( common::message::service::lookup::Request& message)
               {
                  Trace trace{ "service::manager::handle::service::Lookup"};
                  log::line( verbose::log, "message: ", message);

                  auto now = platform::time::clock::type::now();

                  try
                  {
                     // will throw 'Missing' if not found, and a discover will take place
                     auto& service = m_state.service( message.requested);

                     auto handle = service.reserve( now, message.process, message.correlation);

                     if( handle)
                     {
                        auto reply = common::message::reverse::type( message);
                        reply.service = service.information;
                        reply.state = decltype( reply.state)::idle;
                        reply.process = handle;

                        local::optional::send( message.process.ipc, reply);
                     }
                     else if( service.instances.empty())
                     {
                        // note: vi discover on the "real service name", in case it's a route
                        discover( std::move( message), service.information.name, now);
                     }
                     else
                     {
                        auto reply = common::message::reverse::type( message);
                        reply.service = service.information;

                        switch( message.context)
                        {
                           case common::message::service::lookup::Request::Context::no_reply:
                           case common::message::service::lookup::Request::Context::forward:
                           {
                              // The intention is "send and forget", or a plain forward, we use our forward-cache for this
                              reply.process = m_state.forward;

                              // Caller will think that service is idle, that's the whole point
                              // with our forward.
                              reply.state = decltype( reply.state)::idle;

                              local::optional::send( message.process.ipc, reply);

                              break;
                           }
                           case common::message::service::lookup::Request::Context::gateway:
                           {
                              // the request is from some other domain, we'll wait until
                              // the service is idle. That is, we don't need to send timeout and
                              // stuff to the gateway, since the other domain has provided this to
                              // the caller (which of course can differ from our timeouts, if operation
                              // has change the timeout) 
                              // TODO semantics: something we need to address? probably not,
                              // since we can't make it 100% any way...)
                              m_state.pending.requests.emplace_back( std::move( message), now);

                              break;
                           }
                           default:
                           {
                              // send busy-message to caller, to set timeouts and stuff
                              reply.state = decltype( reply.state)::busy;

                              if( local::optional::send( message.process.ipc, reply))
                              {
                                 // All instances are busy, we stack the request
                                 m_state.pending.requests.emplace_back( std::move( message), now);
                              }

                              break;
                           }
                        }
                     }
                  }
                  catch( const state::exception::Missing&)
                  {
                     auto name = message.requested;
                     discover( std::move( message), name, now);
                  }
               }

               void Lookup::discover( 
                  common::message::service::lookup::Request&& message, 
                  const std::string& name, 
                  platform::time::point::type now)
               {
                  Trace trace{ "service::manager::handle::service::Lookup::discover"};

                  auto send_reply = execute::scope( [&]()
                  {
                     log::line( log, "no instances found for service: ", name);

                     // Server that hosts the requested service is not found.
                     // We propagate this by having absent state
                     auto reply = common::message::reverse::type( message);
                     reply.service.name = message.requested;
                     reply.state = decltype( reply.state)::absent;

                     local::optional::send( message.process.ipc, reply);
                  });

                  try
                  {
                     log::line( log, "no instances found for service: ", name, " - action: ask neighbor domains");

                     common::message::gateway::domain::discover::Request request;
                     request.correlation = message.correlation;
                     request.domain = common::domain::identity();
                     request.process = common::process::handle();
                     request.services.push_back( name);

                     // If there is no gateway, this will throw
                     ipc::device().blocking_send( communication::instance::outbound::gateway::manager::optional::device(), request);

                     m_state.pending.requests.emplace_back( std::move( message), now);

                     send_reply.release();
                  }
                  catch( ...)
                  {
                     common::exception::handle();
                  }
               }

               namespace discard
               {
                  void Lookup::operator () ( common::message::service::lookup::discard::Request& message)
                  {
                     Trace trace{ "service::manager::handle::service::discard::Lookup"};
                     log::line( verbose::log, "message: ", message);

                     auto equal_correlation = [&message]( auto& pending)
                     {
                        return message.correlation == pending.request.correlation;
                     };

                     auto reply = message::reverse::type( message);

                     if( auto found = algorithm::find_if( m_state.pending.requests, equal_correlation))
                     {
                        log::line( log, "found pending to discard");
                        log::line( verbose::log, "pending: ", *found);

                        m_state.pending.requests.erase( std::begin( found));
                        reply.state = decltype( reply.state)::discarded;
                     }
                     else 
                     {
                        log::line( log, "failed to find pending to discard - check if we have reserved the service already");

                        // we need to go through all instances.

                        auto reserved_service = [&message]( auto& tuple)
                        {
                           auto& instance = tuple.second;
                           return ! instance.idle() && instance.correlation() == message.correlation;
                        };

                        if( auto found = algorithm::find_if( m_state.instances.sequential, reserved_service))
                        {
                           auto& instance = found->second;
                           log::line( log, "found reserved instance");
                           log::line( verbose::log, "instance: ", instance);

                           instance.discard();

                           reply.state = decltype( reply.state)::replied;
                        }
                     }

                     // We allways send reply
                     local::optional::send( message.process.ipc, reply);
                  }
               } // discard
            } // service

            namespace domain
            {
               namespace discover
               {
                  void Request::operator () ( common::message::gateway::domain::discover::Request& message)
                  {
                     Trace trace{ "service::manager::handle::domain::discover::Request"};
                     common::log::line( verbose::log, "message: ", message);

                     auto reply = common::message::reverse::type( message);

                     reply.process = common::process::handle();
                     reply.domain = common::domain::identity();

                     auto known_services = [&]( auto& name)
                     {
                        auto service = m_state.find_service( name);

                        // We don't allow other domains to access or know about our
                        // admin services.
                        if( service && service->information.category != common::service::category::admin())
                        {
                           if( ! service->instances.sequential.empty())
                           {
                              reply.services.emplace_back(
                                    service->information.name,
                                    service->information.category,
                                    service->information.transaction);
                           }
                           else if( ! service->instances.concurrent.empty())
                           {
                              reply.services.emplace_back(
                                    service->information.name,
                                    service->information.category,
                                    service->information.transaction,
                                    service->instances.concurrent.front().hops());
                           }
                        }
                     };

                     algorithm::for_each( message.services, known_services);

                     ipc::device().blocking_send( message.process.ipc, reply);
                  }

                  void Reply::operator () ( common::message::gateway::domain::discover::accumulated::Reply& message)
                  {
                     Trace trace{ "service::manager::handle::gateway::discover::Reply"};
                     common::log::line( verbose::log, "message: ", message);

                     auto has_correlation = [&message]( auto& pending)
                     {
                        return pending.request.correlation == message.correlation;
                     };

                     if( auto found = algorithm::find_if( m_state.pending.requests, has_correlation))
                     {
                        auto pending = std::move( *found);
                        m_state.pending.requests.erase( std::begin( found));

                        auto service = m_state.find_service( pending.request.requested);

                        if( service && ! service->instances.empty())
                        {
                           // The requested service is now available, use
                           // the lookup to decide how to progress.
                           service::Lookup{ m_state}( pending.request);
                        }
                        else
                        {
                           auto reply = common::message::reverse::type( pending.request);
                           reply.service.name = pending.request.requested;
                           reply.state = decltype( reply.state)::absent;

                           ipc::device().blocking_send( pending.request.process.ipc, reply);
                        }
                     }
                     else
                        log::line( log, "failed to correlate pending request - assume it has been consumed by a recent started local server");
                  }

               } // discover
            } // domain

            void ACK::operator () ( const common::message::service::call::ACK& message)
            {
               Trace trace{ "service::manager::handle::ACK"};
               log::line( verbose::log, "message: ", message);

               try
               {
                  auto now = platform::time::clock::type::now();

                  // This message can only come from a local instance
                  auto& instance = m_state.sequential( message.metric.process.pid);
                  auto service = instance.unreserve( now);

                  // add metric
                  if( m_state.events.active< common::message::event::service::Calls>())
                  {
                     m_state.metric.add( std::move( message.metric));
                     handle::metric::batch::send( m_state);
                  }


                  // Check if there are pending request for services that this
                  // instance has.

                  {
                     auto has_pending = [&instance]( const auto& pending)
                     {
                        return instance.service( pending.request.requested);
                     };

                     if( auto pending = common::algorithm::find_if( m_state.pending.requests, has_pending))
                     {
                        log::line( verbose::log, "found pendig: ", *pending);

                        // We now know that there are one idle server that has advertised the
                        // requested service (we've just marked it as idle...).
                        // We can use the normal request to get the response
                        service::Lookup lookup( m_state);
                        lookup( pending->request);

                        // add pending metrics
                        service->metric.pending += now - pending->when;

                        // Remove pending
                        m_state.pending.requests.erase( std::begin( pending));
                     }
                  }
               }
               catch( const state::exception::Missing&)
               {
                  log::line( log::category::error, "failed to find instance on ACK - indicate inconsistency - action: ignore");
               }
            }

            void Policy::configure( common::server::Arguments& arguments)
            {
               m_state.connect_manager( arguments.services);
            }

            void Policy::reply( common::strong::ipc::id id, common::message::service::call::Reply& message)
            {
               ipc::device().blocking_send( id, message);
            }

            void Policy::ack( const common::message::service::call::ACK& ack)
            {
               handle::ACK handler( m_state);
               handler( ack);
            }

            void Policy::transaction(
                  const common::transaction::ID& trid,
                  const common::server::Service& service,
                  const platform::time::unit& timeout,
                  const platform::time::point::type& now)
            {
               // service-manager doesn't bother with transactions...
            }

            common::message::service::Transaction Policy::transaction( bool commit)
            {
               // service-manager doesn't bother with transactions...
               return {};
            }

            void Policy::forward( common::service::invoke::Forward&& forward, const common::message::service::call::callee::Request& message)
            {
               throw common::exception::xatmi::System{ "can't forward within service-manager"};
            }

            void Policy::statistics( common::strong::ipc::id, common::message::event::service::Call&)
            {
               // We don't collect statistics for the service-manager
            }

         } // handle

         handle::dispatch_type handler( State& state)
         {
            return {
               common::event::listener( handle::process::Exit{ state}),
               handle::process::prepare::Shutdown{ state},
               handle::service::Advertise{ state},
               handle::service::Lookup{ state},
               handle::service::discard::Lookup{ state},
               handle::service::concurrent::Advertise{ state},
               handle::service::concurrent::Metric{ state},
               handle::ACK{ state},
               handle::event::subscription::Begin{ state},
               handle::event::subscription::End{ state},
               handle::Call{ admin::services( state), state},
               handle::domain::discover::Request{ state},
               handle::domain::discover::Reply{ state},
               common::message::handle::Ping{},
               common::message::handle::Shutdown{},
            };
         }
      } // manager
   } // service
} // casual
